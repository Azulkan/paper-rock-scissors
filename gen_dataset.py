''' This script builds a consistent dataset from the 
pictures generated with capture.py
'''

import os
import numpy as np
import cv2
from sklearn.utils import shuffle

np.set_printoptions(suppress=True)

X_paper = np.array([cv2.imread("data/paper/" + filename, cv2.IMREAD_GRAYSCALE).flatten() for filename in os.listdir("data/paper/")])
y_paper = np.ones(int(X_paper.shape[0])) * 0
X_rock = np.array([cv2.imread("data/rock/" + filename, cv2.IMREAD_GRAYSCALE).flatten() for filename in os.listdir("data/rock/")])
y_rock = np.ones(int(X_rock.shape[0])) * 1
X_scissors = np.array([cv2.imread("data/scissors/" + filename, cv2.IMREAD_GRAYSCALE).flatten() for filename in os.listdir("data/scissors/")])
y_scissors = np.ones(int(X_scissors.shape[0])) * 2

X = np.concatenate([X_paper, X_rock, X_scissors])
y = np.concatenate([y_paper, y_rock, y_scissors])

X, y = shuffle(X, y, random_state=0)

print(X.shape)

np.savetxt("dataset/X.csv", X, delimiter=",", fmt="%d")
np.savetxt("dataset/y.csv", y, delimiter=",", fmt="%d")
