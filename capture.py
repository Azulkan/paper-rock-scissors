''' This script allows to generate a important number of
images. 
Usage : python capture.py class [delay]
'''

import sys
import cv2
from time import sleep

def get_image(cam):
    return_value, image = cam.read()
    canny = cv2.Canny(image, 50, 80)
    crop = canny[112:368, 0:256]
    rectangle = cv2.rectangle(image, (0, 112), (256, 368), (0, 255, 0), 2)
    cv2.imshow("image", rectangle)

    return crop

def main(argv):
    cam = cv2.VideoCapture(0)

    if len(sys.argv) == 1:
        print("Wrong number of arguments.")
        exit(0)

    if len(sys.argv) > 1:
        if argv[1] == "paper":
            dest = "data/paper/"
        elif argv[1] == "rock":
            dest = "data/rock/"
        elif argv[1] == "scissors":
            dest = "data/scissors/"
        else:
            print("Argument 1 must be : paper | rock | scissors.")
            exit(0)

        if len(sys.argv) > 2:
            delay = argv[2]
        else:
            delay = None

    count = 0


    if delay is None:
        while True:
            crop = get_image(cam)
            k = cv2.waitKey(1)
            
            if k == ord('s'):
                cv2.imwrite(dest + str(count) + ".jpg", crop)
                count += 1
            elif k == 27 or k == ord('q'):
                break

    else:
        while True:
            crop = get_image(cam)
            k = cv2.waitKey(1)
        
            if k == ord('s'):
                while True:
                    crop = get_image(cam)
                    k = cv2.waitKey(1)
                    if k == 27 or k == ord('q'):
                        break
                    else:
                        cv2.imwrite(dest + str(count) + ".jpg", crop)
                        count += 1
                        sleep(float(delay)/1000)

    cam.release()
    cv2.destroyAllWindows()

if __name__ == '__main__':
  main(sys.argv)