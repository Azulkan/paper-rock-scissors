''' This script re-instanciate the model with the saved weights, 
opens a window that captures from the webcam, applies canny filter
and predicts the class from the cropped image.
'''

import numpy as np

import keras
from keras.models import model_from_json
from keras import backend as K

import cv2

# input image dimensions
img_rows, img_cols = 256, 256

if K.image_data_format() == 'channels_first':
    input_shape = (1, img_rows, img_cols)
else:
    input_shape = (img_rows, img_cols, 1)


# load json and create model
json_file = open('model/model.json', 'r')
model_json = json_file.read()
json_file.close()
model = model_from_json(model_json)
# load weights into new model
model.load_weights("model/weights.h5")

model.compile(loss=keras.losses.categorical_crossentropy,
              optimizer=keras.optimizers.Adadelta(),
              metrics=['accuracy'])

cam = cv2.VideoCapture(0)

while True:
    return_value, image = cam.read()
    canny = cv2.Canny(image, 50, 80)
    x = canny[112:368, 0:256]
    rectangle = cv2.rectangle(image, (0, 112), (256, 368), (0, 255, 0), 2)
    cv2.imshow("image", rectangle)

    if K.image_data_format() == 'channels_first':
        x = x.reshape(1, img_rows, img_cols)
    else:
        x = x.reshape(img_rows, img_cols, 1)

    y = model.predict_classes(np.array([x]))
    
    if y == 0:
        print("Paper")
    elif y == 1:
        print("Rock")
    elif y == 2:
        print("Scissors")

    k = cv2.waitKey(1)
    if k == 27 or k == ord('q'):
        break

cam.release()
cv2.destroyAllWindows()